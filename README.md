##draft code:

```

#include "Joystick.h"



#define NOFIELD 505L    // Analog output with no applied field, calibrate this
// Uncomment one of the lines below according to device in use A1301 or A1302
// This is used to convert the analog voltage reading to milliGauss
#define TOMILLIGAUSS 1953L  // For A1301: 2.5mV = 1Gauss, and 1024 analog steps = 5V, so 1 step = 1953mG
// #define TOMILLIGAUSS 3756L  // For A1302: 1.3mV = 1Gauss, and 1024 analog steps = 5V, so 1 step = 3756mG

const byte averageFactor = 10;

int hallPin = 1;    // select the input pin for the hall
int potPin = 2;    // select the input pin for the potentiometer
int value = 0;       // variable to store the value coming from the sensor
int timer = 0;
int curspeed = 0;
double halltime = 0;
int oldval=0;


float radius = 13.5;//Радиус шины в дюймах. У 26 дюймовой шины, около 13.5 (магия)
float deltaD;//Проезжаем за оборот в метрах
float circumference;
int maxReedCounter = 100;//Минимальное время в миллисекундах на оборот
int reedCounter;
boolean moving = false; //флаг - едем или нет
float kmh = 0.00;//Скорость в км/ч
float distance = 0;//Дистанция поездки в метрах

bool done = false;
int delta;
double oldmillis = 0;

void setup() {
  //digitalWrite(A1, INPUT_PULLUP);  // set pullup on analog pin 1 hall
  //digitalWrite(A2, INPUT_PULLUP);  // set pullup on analog pin 2 pot
    Joystick.begin();
    Serial.begin(9600);
    reedCounter = maxReedCounter;
}

void loop() {
  circumference = 2*3.14*radius;//То же самое, но с меньшей точностью (для измерения 

  value = DoMeasurement();
  
  if (value){
     if (millis() - halltime > 20 && done == false){
      done = true;
      curspeed = (millis() - halltime);
      
      if (curspeed>2000 )curspeed = 2000;
      
      delta = (millis() - halltime);
      kmh = (56.8*float(circumference))/float(delta)*1.61;//километры в час
      //kmh = map(curspeed,2000,0,0,72);
      
      halltime = millis();
      
      //Serial.println(kmh);
     }
  } else 
  {
    done = false;
  }
  
  if (millis() - halltime>2000)
    kmh = 0;
  

  

  
  int val= map(kmh,0,300,0,-127);
  if (val < -127) val = -127;  
  
  if (averageFactor > 0)        // усреднение показаний для устранения "скачков"
  {     
    int oldval = val;
    val = (oldval * (averageFactor - 1) + val) / averageFactor; 
  }


  Serial.println(val);
  Joystick.setThrottle(val);
  Joystick.setYAxis(val);
  
  oldval = val;
  
  /*
  value = analogRead(potPin);
  int steering= map(value,0,1023,127,-127);
  Joystick.setXAxis(steering);
  */

  oldmillis = millis();  
}



bool DoMeasurement()
{
// measure magnetic field
  int raw = analogRead(hallPin);   // Range : 0..1024

//  Uncomment this to get a raw reading for calibration of no-field point
//  Serial.print("Raw reading: ");
//  Serial.println(raw);

  long compensated = raw - NOFIELD;                 // adjust relative to no applied field 
  long gauss = compensated * TOMILLIGAUSS / 1000;   // adjust scale to Gauss

  //Serial.print(gauss);
  //Serial.print(" Gauss ");

  //if (gauss > 0)     Serial.println("(South pole)");
  //else if(gauss < 0) Serial.println("(North pole)");
  //else               Serial.println();
  
  if (gauss > 0)     return false;
  else               return true;
  
}


```










# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact